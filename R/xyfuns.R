#' @import dplyr
#' @import reshape2
#' @import epiR
#' @export
xyfun = function(df){
  qt = levels(df$xy)

  result = df %>% group_by(rowSeq, colSeq) %>% do({
    dfx = subset(. , xy == qt[1]) %>% arrange(idvar)
    dfy = subset(. , xy == qt[2]) %>% arrange(idvar)
    r = try( cor(dfx$value, dfy$value))
    if (class(r) == "try-error"){
      r = NaN
    }
    ccc = try(epi.ccc(dfx$value, dfy$value))
    if (class(ccc) == "try-error")  {
      lcc = NaN
      lbias = NaN
    } else {
      lcc = as.numeric(ccc$rho.c[1])
      lbias = as.numeric(ccc$C.b)
    }
    data.frame(r = r, lcc = lcc, lbias = lbias)


  })
  return(result)
}
